/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author olave
 */
public class Actualizar {
   public int idcliente;
    public String  nombre;
                public int  precio;
                public String descripcion;
                 public String fecha;
    public  Actualizar( int idcliente,String nombre,int precio, String descripcion, String fecha){//CONSTRUCTOR
         this.idcliente=idcliente;
         this.nombre=nombre;
        this.precio=precio;
        this.descripcion=descripcion;
        this.fecha=fecha;
    
    try{//INTENTAR CONEXIÓN A BASE DE DATOS
             
            Connection con;
            con = Conexion.getConexion();
            //REALIZAR PETICION SP EN SQL
            PreparedStatement ps = con.prepareStatement("UPDATE producto SET pro_nombre=?, pro_precio=?, pro_descripcion=? WHERE pro_id=?");
            
            ps.setString(1, nombre);//ENVIAR ARGUMENTOS
            ps.setInt(2, precio);
            ps.setString(3,descripcion);
            
            ps.setInt(4, idcliente);
            int rowsDeleted = ps.executeUpdate();
if (rowsDeleted > 0) {
    JOptionPane.showMessageDialog(null,"Registro guardado");//MENSAJE SI HAY CONFIRMACIÓN
}
            
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,ex.toString());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Insertar.class.getName()).log(Level.SEVERE, null, ex);
        } 
}
}