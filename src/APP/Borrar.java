/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author olave
 */
public class Borrar {
    public int idcliente;
    
    public  Borrar( int idcliente){
         this.idcliente=idcliente;
         
    
    try{
             
            Connection con;
            con = Conexion.getConexion();
            PreparedStatement ps = con.prepareStatement("DELETE FROM producto WHERE pro_id=?");//PETICIÓN SP EN SQL
            ps.setInt(1,idcliente);//enviar argumentos
            
            //ps.setString(6, "1");
            
            int rowsDeleted = ps.executeUpdate();
if (rowsDeleted > 0) {
    JOptionPane.showMessageDialog(null,"Registro guardado");//MOSTRAR MENSAJE de confirmación
}
            
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,ex.toString());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Insertar.class.getName()).log(Level.SEVERE, null, ex);
        }
}}
