create table producto(

    pro_id  int primary key,

    pro_nombre  varchar(20),

    pro_precio int,

    pro_descripcion VARCHAR(MAX),

    pro_fecha_vencimiento date

);